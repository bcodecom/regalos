package com.example.regalosv3.ui.sendInv;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SendInvViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SendInvViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is send invitations fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
