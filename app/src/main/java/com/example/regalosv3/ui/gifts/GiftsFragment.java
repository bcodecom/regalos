package com.example.regalosv3.ui.gifts;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.regalosv3.R;
import com.example.regalosv3.ui.home.HomeViewModel;

public class GiftsFragment extends Fragment {

    private GiftsViewModel giftsViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        giftsViewModel =
                ViewModelProviders.of(this).get(GiftsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_gifts, container, false);
        final TextView textView = root.findViewById(R.id.text_gifts);
        giftsViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

}
